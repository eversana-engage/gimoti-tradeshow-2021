# README

Single page to display thumbnails for videos and open them in a modal.

### How do I get set up?

- If you can copy all the files from the USB drive to a folder on the laptop, please do so.
- If not, continue to work with the files on the USB drive.
- Open the folder that has the files in it.
- Open the index.html file in your browser with Chrome.
- Drag the Chrome window to the TV screen.
- Make the browser full screen by clicking the F11 key on the keyboard.
- If there is a Windows task bar on the screen, right click it, open task bar settings and set the taskbar to 'hide'.

